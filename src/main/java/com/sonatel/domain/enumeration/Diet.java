package com.sonatel.domain.enumeration;

/**
 * The Diet enumeration.
 */
public enum Diet {
    HERBIVORE, CARNIVORE, HOMNIVORE
}
