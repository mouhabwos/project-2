package com.sonatel.repository;

import com.sonatel.domain.Animal;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Animal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnimalRepository  {

     public  Animal save(Animal animal);



    public  void delete(String id);

    public List<Animal> findAll();

    public Animal findById(String id);

    public Animal findByName(String name);



}
