package com.sonatel.repository.impl;

import com.sonatel.domain.Animal;
import com.sonatel.domain.enumeration.Diet;
import com.sonatel.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import rx.functions.ActionN;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class AnimalRepositoryImpl implements AnimalRepository {
    @Autowired
    private EntityManager em;



    public  Animal save(Animal animal){

        em.persist(animal);
        em.flush();
        return animal;

    }



    public  void delete(String id){

        em.createQuery("Delete from Animal  a where a.id = " + id).executeUpdate();



    }

    public  List<Animal> findAll(){

        return em.createQuery("Select a from Animal a",Animal.class).getResultList();

    }

    public Animal findById(String id){

        return (Animal) em.createQuery("Select a from Animal a where a.id = " + id).getSingleResult();

    }

    public Animal findByName(String name){
        System.out.println("+++++++++++++++++++++++++++++++ name... "+name);


        Animal result = null;
        try {
            result = (Animal) em.createNativeQuery("Select * from animal a where a.name ='" + name + "'",Animal.class).getSingleResult();
        } catch (NoResultException e) {
            System.out.println("=========================No result forund for... ");
        }
        return  result;

    }

}
