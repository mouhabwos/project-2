package com.sonatel.service;

import com.sonatel.domain.Animal;

import java.util.List;

/**
 * Service Interface for managing Animal.
 */
public interface AnimalService {

    /**
     * Save a animal.
     *
     * @param animalDTO the entity to save
     * @return the persisted entity
     */
    Animal save(Animal animalDTO) throws  Exception;

    /**
     * Get all the animals.
     *
     * @return the list of entities
     */
    List<Animal> findAll();


    /**
     * Get the "id" animal.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Animal findOne(String id);

    /**
     * Delete the "id" animal.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Load a animal.
     *
     * @return the persisted entity
     */
    void load() ;

    /**
     * Get the "name" animal.
     *
     * @param name the name of the animal
     * @return the entity
     */
    Animal findByName(String name );
}
