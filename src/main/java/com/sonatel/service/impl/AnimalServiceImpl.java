package com.sonatel.service.impl;

import com.sonatel.config.ApplicationProperties;
import com.sonatel.domain.enumeration.Diet;
import com.sonatel.service.AnimalService;
import com.sonatel.domain.Animal;
import com.sonatel.repository.AnimalRepository;
 import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Animal.
 */
@Service
@Transactional
public class AnimalServiceImpl implements AnimalService {

    private final Logger log = LoggerFactory.getLogger(AnimalServiceImpl.class);

    private final AnimalRepository animalRepository;

     private final ApplicationProperties properties;


    public AnimalServiceImpl(AnimalRepository animalRepository,ApplicationProperties properties) {
        this.animalRepository = animalRepository;
        this.properties = properties;
    }

    /**
     * Save a animal.
     *
     * @param animal the entity to save
     * @return the persisted entity
     */
    @Override
    public Animal save(Animal animal) throws Exception {

        Animal animalExist = findByName(animal.getName());

       // log.debug("Request to save Animal : {}", animalExist.getName());

        if (animalExist!=null){
            throw  new Exception(" L'animal existe !!!!");
        }
        return animalRepository.save(animal);
    }

   
    @Override
    @Transactional(readOnly = true)
    public List<Animal> findAll() {
        log.debug("Request to get all Animals");
        return animalRepository.findAll();
    }


    /**
     * Get one animal by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Animal findOne(String id) {
        log.debug("Request to get Animal : {}", id);
        return animalRepository.findById(id);
    }

    /**
     * Delete the animal by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Animal : {}", id);
        animalRepository.delete(id);
    }
    /**
     * Load a animals.
     *
     */
    public void load()  {

          String FILENAME = "/Users/sow028628/Documents/animal.txt";
        BufferedReader br = null;
        FileReader fr = null;

       try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                String[] fields =sCurrentLine.split(";");

                Animal animal= new Animal(fields[0],fields[1], Diet.valueOf(fields[2]));
                save(animal);
            }

        } catch (Exception e) {
           e.printStackTrace();
        }
    }


    @Override
    public Animal findByName(String name) {
        return animalRepository.findByName(name);
    }

}
