package com.sonatel.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sonatel.domain.Animal;
import com.sonatel.service.AnimalService;
import com.sonatel.web.rest.errors.BadRequestAlertException;
import com.sonatel.web.rest.util.HeaderUtil;
import com.sonatel.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Animal.
 */
@RestController
@RequestMapping("/api")
public class AnimalResource {

    private final Logger log = LoggerFactory.getLogger(AnimalResource.class);

    private static final String ENTITY_NAME = "projet2Animal";

    private final AnimalService animalService;

    public AnimalResource(AnimalService animalService) {
        this.animalService = animalService;
    }

    @PostMapping("/animals")
    @Timed
    public ResponseEntity<Animal> createAnimal(@RequestBody Animal animal) throws URISyntaxException {
        log.debug("REST request to save Animal : {}", animal.getName());
        Animal a =null;
        try {
             a = animalService.save(animal);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return new ResponseEntity<>(a, HttpStatus.OK);

    }


    @GetMapping("/animals")
    @Timed
    public ResponseEntity<List<Animal>> getAllAnimals() {
        log.debug("REST request to get a page of Animals");
        return new ResponseEntity<>(animalService.findAll(), HttpStatus.OK);
    }


    @GetMapping("/animals/{id}")
    @Timed
    public ResponseEntity<Animal> getAnimal(@PathVariable String id) {
        log.debug("REST request to get Animal : {}", id);
        Animal animal = animalService.findOne(id);
        return new ResponseEntity<>(animal, HttpStatus.OK);
    }


    @DeleteMapping("/animals/{id}")
    @Timed
    public ResponseEntity<Void> deleteAnimal(@PathVariable String id) {
        log.debug("REST request to delete Animal : {}", id);
        animalService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @PostMapping("/animals/load")
    @Timed
    public ResponseEntity<Animal> charger()  {
        animalService.load();


        return ResponseEntity.ok().build();
    }
}
