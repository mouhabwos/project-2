/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sonatel.web.rest.vm;
